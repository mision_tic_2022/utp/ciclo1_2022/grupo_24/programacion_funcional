
guion = lambda : print('--------------------------------------------------')

numeros = [10,20,30,40,50,60,70,80,90]

numeros_al_cuadrado = [n**2 for n in numeros ]

print(numeros_al_cuadrado)

nombres = ['Andrés', 'Juliana', 'Claudia', 'Oliver', 'Bladimir']

print( nombres[1] )

iniciales = [n[0] for n in nombres ]
print(iniciales)

guion()
nombres_mayuscula = [n.upper() for n in nombres ]
print(nombres_mayuscula)

guion()

formula = lambda n: (n+10)*2

lista = [ formula(n) for n in numeros ]
print(lista)


nombres = ['Andrés', 'Juliana', 'Claudia', 'Oliver', 'Bladimir']

'''
A partir de la lista de nombres genere una lista de tuplas, convirtiendo 
cada nombre a una tupla
'''
#Solución de Luis Vergara
convertir_tupla = [tuple(n) for n in nombres]

guion()
numeros = [10,20,30,40,50,60,70,80,90,15,25,35,45,55,65,75,85,95]

numeros_pares = [n for n in numeros if n%2==0]
print(numeros_pares)
guion()
numeros_impares = [n for n in numeros if n%2!=0 ]
print(numeros_impares)

guion()
numeros_impares = [n if n%2==0 else 0 for n in numeros ]
print(numeros_impares)

guion()
nombres = ['Andrés', 'Juliana', 'Claudia', 'Oliver', 'Bladimir', 'Juan', 'Jorge', 'Julieta']

nombres_x_j = [n.upper() for n in nombres if n[0].lower()=='j' ]
print(nombres_x_j)

'''
* Generar una lista con los nombres que empiezan por j y guardarlos en minúscula, de 
    lo contrario guardarlos en mayúscula
'''
lista = [n.lower() if n[0].lower()=='j' else n.upper() for n in nombres ]








#Solución de Jeison Esteban
#nombres_j = [n.lower() if n[0].lower() == 'j' else n.upper() for n in nombres]


