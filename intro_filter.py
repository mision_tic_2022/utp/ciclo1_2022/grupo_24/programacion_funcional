
numeros = [10,15,20,25,30,35,40,45,50,55]

pares = lambda n: n%2 == 0
impares = lambda n: n%2 != 0

respuesta = list(filter( pares, numeros ))
print(respuesta)
respuesta = list(filter(impares, numeros))
print(respuesta)

'''
1) A partir de la lista 'nombres' filtrar todos los nombres que comiencen por J
'''
nombres = ['Andres', 'Juan', 'Juliana', 'Jorge', 'Iván', 'Hugo', 'Oliver', 'Jairo']

#Jeison Esteban
funcion = lambda name: name[0].lower() == 'j'
nombres_filtrados = list(filter(funcion, nombres))
print(nombres_filtrados)

#Jean Carlos
mayuscula = lambda name: name[0]=='J'
list_mayus = list(filter(mayuscula, nombres_filtrados))
print(list_mayus)